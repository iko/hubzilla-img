<?php
/*
 * Name: Red Emoji
 * Description: A light-hearted emoji pack.
 * Version: 1.0
 * Author: Iko
 * 
 * Usage: type two colons followed by the emoji name, e.g. ::smile 
 * To change the trigger prefix ("::"), edit the $prefix variable.
 *
 */

function red_emoji_load() {
    register_hook('emoji', 'addon/red_emoji/red_emoji.php', 'red_emoji');
}

function red_emoji_unload() {
    unregister_hook('emoji', 'addon/red_emoji/red_emoji.php', 'red_emoji');
}

function red_emoji(&$a,&$b){

    $prefix = '::';
    $path = '/addon/red_emoji/';
 
    $sets = array(
        'core' => array('angelic', 'angry1', 'angry2', 'clap', 'confused', 'cool', 'crap', 'cry1', 'cry2', 'dead', 'dead2', 'devilish', 'dislike', 'drool', 'embarrassed', 'happy1', 'happy2', 'heartbreak', 'humph1', 'humph2', 'idea', 'like', 'lol', 'love1', 'love2', 'love3', 'mock', 'music', 'no', 'notice', 'ow', 'pokerface', 'question', 'razz', 'redmatrix', 'sad1', 'sad2', 'sick', 'sigh', 'smile', 'smirk', 'smoke', 'starstruck', 'surprised', 'sweatdrop1', 'sweatdrop2', 'tired', 'victory', 'wink', 'worried', 'yes', 'zzz')
    );
 
    foreach ($sets as $key => $st) {
        foreach ($st as $em) {
                $b['texts'][] = $prefix . $em;
                $b['icons'][] = '<img class="red-emoji" src="' . z_root() . $path . $key . '/' . $em . '.png" alt="' . $prefix . $em . '" />';
        }
    }
}

